"""
Revision: $Id$
"""

### Imports ###
from distutils.core import setup
import py2exe
import sys
import os

### Constants ###
SOURCE_DIR = r'..\src'
BUILD_TEMP = r'..\build_temp'
OUTPUT_DIR = r'..\bin'

### Code ###
# If run without args, build executables, in quiet mode.
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
    sys.argv.append("-q")

os.chdir(SOURCE_DIR)
sys.path.append(os.getcwd())

# Create the setup file
setup(
	options = {"py2exe": {"compressed": 1,
						  "optimize": 2,
						  "ascii": 1,
						  "bundle_files": 1, 
						  "dist_dir": os.path.abspath(OUTPUT_DIR),	
						  }},
	zipfile = None,
	windows = [{'script': "main.py",
				'version': "1.0.0",
				'name': "Orazi Game",
				'dest_base': "orazi",
				'icon_resources': [(1, "../media/Orazi.ico")],
				"packages": ["OpenGL.GL","OpenGL.GLU"],
			   }],
)

print 
print "***"
print "Build done."
print "You may want to delete the temporary 'build' subdir under 'src'"
