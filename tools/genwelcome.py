"""
Module: citygen
Purpose: Automatic generation of city map
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import sys
sys.path.append('../src')

import config
from citygen import CityMap
import random

### Constants ###
WELCOME_IMAGE_SRC = '../media/welcome-base.bmp'
WELCOME_IMAGE_TARGET = '../media/welcome.png'

### Code ###
welcomeMap = CityMap(100, 100)
welcomeMap.loadBitmap(WELCOME_IMAGE_SRC)

# Complete the map by putting buildings & parks in the white areas
welcomeMap.generate(False)

# Generate park colors
for y in range(welcomeMap.getHeight()):
	for x in range(welcomeMap.getWidth()):
		tile = welcomeMap.getSquare(x,y)
		if tile[0] == welcomeMap.MAP_ITEM_PARK:
			welcomeMap.setSquareParam(x,y,int(255-60*random.random()))
		
welcomeMap.saveInternalBitmap(WELCOME_IMAGE_TARGET)
welcomeMap.getColoredBitmap(4).show()
