"""
Module: gametime
Purpose: Game time tracking & management
Author: Nir Hershko
Revision: $Id$

Global functions used to handle global timings:
	* Time of each frame
	* Duration of play in a game and in a level
	* Duration of running time in a level (can be paused)
		[currently no support for time compression]
"""

### Imports ###
import time

import config

from scenelevel import SceneLevel

### Global Variables ###
#first initialize the frame timers
g_current_frame = time.time()
g_last_frame = time.time()
#initialize the FPS counters
g_frame_count = 0
g_frame_count_time = time.time()
g_frames_per_second = 0

g_running_level_time = 0

### Global Functions ###
#Update the timings according to events (game, level, frame)
def initializeGame():
	global g_start_game
	#set the game timestamp
	g_start_game = time.time()
def initializeLevel():
	global g_start_level, g_running_level_time
	#set the level timestamp
	g_start_level = time.time()
	#initialize the level running time
	g_running_level_time = 0
def updateFrame():
	global g_current_frame, g_last_frame, g_running_level_time
	global g_frame_count, g_frame_count_time, g_frames_per_second
	#add the frame time to the level running time
	if isinstance(config.current_level,SceneLevel) and config.current_level.is_active():
		g_running_level_time += getFrameDelta()
	#set the new timestamps
	g_last_frame = g_current_frame
	g_current_frame = time.time()
	#calculate the FPS
	g_frame_count += 1
	if g_current_frame > g_frame_count_time+1:
		config.current_level.update_gui_content()
		if config.print_debug:
			print "FPS: %d" % g_frame_count
		g_frame_count_time = g_current_frame
		g_frames_per_second = g_frame_count
		g_frame_count = 0

#Receive information about timing
def getFramesPerSecond():
	return g_frames_per_second
def getFrameDelta():
	return g_current_frame - g_last_frame
def getFrameCount(): #since the start of the second
	return g_frame_count
def getLevelTime():
	return g_current_frame - g_start_level
def getLevelRunningTime():
	return g_running_level_time
def getGameTime():
	return g_current_frame - g_start_game
