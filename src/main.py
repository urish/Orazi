"""
Module: main
Purpose: Game dispatcher
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import pygame

# The following hack makes opengl work with py2exe
g_originalOpen = open
from StringIO import StringIO
def newOpen(fileName, *args):
	if fileName.endswith('version'):
		return StringIO('2.0.2.01')
	return g_originalOpen(fileName, *args)
__builtins__.open = newOpen
from OpenGL import GL
__builtins__.open = g_originalOpen

# Additional imports
import os
import sys
import gametime
import loadinglevel
import config
import sound

# Another hack - make PIL work correctly with Py2EXE
import Image
import PngImagePlugin
Image._initialized=2

### Code ###
def main():
	# Take advantage of psyco, if available.
	try:
		import psyco
		assert psyco.__version__ >= 17105392
		psyco.profile(memory = 32768) #use maximum of 32 megs of memory for psyco data structures
	except ImportError:
		print "Install psyco 1.5.1 (or better) for best performance"

	pygame.display.init()
	pygame.font.init()
	
	pygame.display.set_caption("Where's Orazi?")
	flags = pygame.DOUBLEBUF | pygame.OPENGL 
	if '-w' not in sys.argv:
		flags |= pygame.FULLSCREEN
	
	window = pygame.display.set_mode((800,600),flags)
	screen = pygame.display.get_surface()
	loadingLevel = loadinglevel.LoadingLevel()
	
	#GL.glDepthRange(0.5,100)
	
	def doLevel(level):
		# Display the 'loading level' message
		loadingLevel.handle_event(pygame.event.Event(pygame.VIDEOEXPOSE,{}))
		if level.level_state == level.LEVEL_STATE_INIT:
			level.start()
		
			# Check if there's an intro screen for the level
			introScreen = level.get_intro()
			if introScreen:
				level = introScreen
				if level.level_state == level.LEVEL_STATE_INIT:
					level.start()
		
		gametime.initializeLevel()
		
		# Start the level's event loop
		level.level_state = level.LEVEL_STATE_ACTIVE
		while level.level_state == level.LEVEL_STATE_ACTIVE:
			events = pygame.event.get()
			if [] == events:
				pygame.event.post(pygame.event.Event(pygame.VIDEOEXPOSE,{}))
			else:
				for event in events:
					level.handle_event(event)
					
		return level.get_next_level()

	config.initialization()
	gametime.initializeGame()
	sound.initSound()
	level = config.first_level()
	while level != None:
		config.current_level = level
		level = doLevel(level)

if __name__ == "__main__":
	main()
