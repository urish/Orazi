"""
Module: config
Purpose: Holds some configuration variables
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import sys
import random
import itertools

from textures import getTexture
import citygen
import gametime
import testmodel
import menu
import model
import welcome
import findorazilevel
import avoidorazilevel

### Constants ###
print_debug = False

#radians, from the top. 0 is no movement
MINIMUM_HEIGHT_ANGLE = 0.6
MINIMUM_CAMERA_DISTANCE = 2.4
MAXIMUM_CAMERA_DISTANCE = 16.3
CAMERA_DISTANCE_SPEED = 2.4 #exponential per second
CAMERA_DISTANCE_SCROLL = 1.2 #exponential per scroll
CAMERA_ROTATION_SPEED = 1.3 #degrees/second

MINIMUM_ORAZI_START_DISTANCE = 10	

max_tiles_on_screen = 10

GAME_LEVELS = [
	('Welcome',    welcome.WelcomeLevel),
	('Find Orazi', findorazilevel.FindOraziLevel),
	('Avoid Orazi', avoidorazilevel.AvoidOraziLevel),
]

FIND_ORAZI_TIME_LIMIT = 600 #seconds
AVOID_ORAZI_TIME_LIMIT = 300

def first_level():
	# The following are handy throughout the development process
	if 'menu' in sys.argv:
		return menu.MenuLevel.getInstance()
	if 'welcome' in sys.argv:
		return welcome.WelcomeLevel()
	if 'test_model' in sys.argv:
		return testmodel.TestModelLevel()
	if 'find_orazi' in sys.argv:
		return avoidorazilevel.AvoidOraziLevel()
	# Default
	return welcome.WelcomeLevel()

current_level = None

# Game Over overlays
LEVEL_OVERLAY_WIN  = '../media/LevelWin.png'
LEVEL_OVERLAY_LOSE = '../media/LevelLose.png'

#for debugging purposes
g_counter = 0
test_object = None

MINIMUM_REASONABLE_MAP_SIZE = 30

# Functions to generate different building shapes & rotate them (using cryptic lambda syntax)
_FillBuilding = lambda w, h: zip(range(w) * h, 
								 itertools.chain(*[[i]*w for i in range(h)]))
_LBuilding = lambda h, w: [(x, 0) for x in range(w)] + [(0, y) for y in range(1, h)]
_CBuilding = lambda h, w: (zip(range(w) * 2, [0] * w + [h-1] * w) + 
							   [(0, y) for y in range(1, h - 1)])
_RotateBuilding90C = lambda building: [(max([_y for _x,_y in building])-y, x) 
											for x, y in building]

#buildings:
buildings = [
	(_FillBuilding(1,1), 1, (1,1)),
	(_FillBuilding(2,2), 1, (2,2)),
	(_FillBuilding(1,2), 1, (1,2)),
	(_FillBuilding(3,2), 1, (3,2)),
	(_FillBuilding(3,3), 1, (3,3)),
	(_FillBuilding(1,1), 0.64, (1,1)),
	(_FillBuilding(2,2), 0.82, (2,2)),
	(_FillBuilding(1,2), 0.44, (1,2)),
	(_FillBuilding(3,2), 0.75, (3,2)),
	(_FillBuilding(3,3), 0.66, (3,3)),
]

#NPCs
def get_person_speed():
	return 0.4+0.05*random.random()
#player
rotation_speed = 97 #degrees / second
player_speed = get_person_speed()*3.7

sign_directions = {0:(-1,0), 1:(0,+1), 2:(+1,0), 3:(0,-1)}

RANDOM_COLORS = [(1,0.5,0.5),(0.5,1,0.5),(0.5,0.5,1),(0.5,1,1),(1,0.5,1),(1,1,0.5)]
def random_color():
	return RANDOM_COLORS[int(len(RANDOM_COLORS)*random.random())]

road_tiles = {
	citygen.CityMap.ROAD_TYPE_STRAIGHT: "../model/roads/kvish/straight.png",
	citygen.CityMap.ROAD_TYPE_TURN: 	"../model/roads/kvish/turn.png",
	citygen.CityMap.ROAD_TYPE_TEE:		"../model/roads/kvish/tee.png",
	citygen.CityMap.ROAD_TYPE_CROSS:	"../model/roads/kvish/cross.png",
	citygen.CityMap.ROAD_TYPE_TERMINAL: "../model/roads/kvish/terminal.png",
}

park_tiles = ["../model/park/park1.png"]

park_elements = [
	"../model/park/elements/element1.png",
	"../model/park/elements/element2.png",
]

PARK_OBJECT_CHANCE = 0.3 #of 1
PARK_OBJECT_COUNT = len(park_elements)

# Define the special welcome-screen elements
welcome_elements = [
	"../model/park/elements/welcome_left.png",
	"../model/park/elements/welcome_right.png",
]
park_elements += welcome_elements
WELCOME_ELEMENT_LEFT  = park_elements.index(welcome_elements[0])
WELCOME_ELEMENT_RIGHT = park_elements.index(welcome_elements[1])

sign_tile = "../model/roads/kvish/sign.png"

#done after loading pygame and opengl
def initialization():
	global road_tiles
	global park_tiles, park_elements
	global sign_tile
	
	for index, name in road_tiles.items():
		road_tiles[index] = getTexture(name)
	for index, name in enumerate(park_tiles):
		park_tiles[index] = getTexture(name)
	for index, name in enumerate(park_elements):
		park_elements[index] = getTexture(name)
	sign_tile = getTexture(sign_tile)
	
	#for index, building in enumerate(buildings):
	#	buildings[index] = (building[0],model.getModel(*building[1]))

