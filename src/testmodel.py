import math

from OpenGL import GL,GLU

from level import Level
import model
import config

class TestModelLevel(Level):
	def start(self):
		Level.__init__(self)
		
		self.test_model = model.Model("buildings","02","Untitled.obj")
		
		self.height_angle = 1
		self.rotation_angle = 0.2
		self.camera_distance = 16.3
		self.look_at = (0,0)
		
	def draw(self, frameDelta):
		Level.draw(self, frameDelta)
		
		eyex = self.camera_distance * math.sin(self.height_angle) * math.sin(self.rotation_angle)
		eyey = self.camera_distance * math.sin(self.height_angle) * math.cos(self.rotation_angle)
		eyez = self.camera_distance * math.cos(self.height_angle)
		
		eyex += self.look_at[0]
		eyey += self.look_at[1]
		
		GL.glMatrixMode(GL.GL_PROJECTION)
		GLU.gluPerspective(30,800.0/600.0,1,300)
		GL.glMatrixMode(GL.GL_MODELVIEW)
		
		GL.glEnable(GL.GL_DEPTH_TEST)
		
		GL.glEnable(GL.GL_LIGHT0)
		GL.glLightfv(GL.GL_LIGHT0,GL.GL_POSITION,(-1,-1,-1,0))
		GL.glEnable(GL.GL_LIGHT1)
		GL.glLightfv(GL.GL_LIGHT1,GL.GL_POSITION,(-1,-1,-1,0))
		
		GL.glLoadIdentity()
		GLU.gluLookAt(eyex,eyey,eyez,
					  self.look_at[0],self.look_at[1],0,
					  0,0,1)
		
		#GL.glPushMatrix()
		#GL.glScalef(0.01,0.01,0.01)
		self.test_model.draw()
		#GL.glPopMatrix()
		
	def handle_mouse_motion(self, pos, rel, buttons):
		if buttons[2]:
			self.rotation_angle += rel[0]*0.01
			self.rotation_angle = self.rotation_angle % (2*math.pi)
			
			self.height_angle += rel[1]*0.01
			if self.height_angle < 0.001:
				self.height_angle = 0.001
			elif self.height_angle > config.MINIMUM_HEIGHT_ANGLE:
				self.height_angle = config.MINIMUM_HEIGHT_ANGLE
	
	def handle_mouse_down(self, pos, button):
		if button == 4:
			self.camera_distance /= 1.1
		if button == 5:
			self.camera_distance *= 1.1
	
