"""
Module: tiledobjectmanager
Purpose: 
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import Image

### Class TiledObjectsManager ####
class TiledObjectsManager:
	def __init__(self, width, height):
		self.dimensions = (width, height)
		self.bitmap = Image.new('I', self.dimensions, -1)
		self.heap = []
	
	def get_objects(self, x, y):
		if (x < 0) or (x >= self.dimensions[0]):
			return []
		if (y < 0) or (y >= self.dimensions[1]):
			return []
		index = self.bitmap.getpixel((x,y))
		if index == -1:
			return []
		data = self.heap[index]
		if data[0] != (x,y):
			raise Exception("Sanity check failed, mismatch of heap and bitmap position")
		return data[1]
	
	def add_object(self, x, y, object):
		if (x < 0) or (x >= self.dimensions[0]):
			raise Exception("Cannot add object: invalid position (%d,%d)" % (x,y))
		if (y < 0) or (y >= self.dimensions[1]):
			raise Exception("Cannot add object: invalid position (%d,%d)" % (x,y))
		index = self.bitmap.getpixel((x,y))
		if index == -1:
			data = ((x,y),[object])
			index = len(self.heap)
			self.heap.append(data)
			self.bitmap.putpixel((x,y),index)
		else:
			data = self.heap[index]
			if data[0] != (x,y):
				raise Exception("Sanity check failed, mismatch of heap and bitmap position")
			data[1].append(object)
	
	def remove_object(self, x, y, object):
		if (x < 0) or (x >= self.dimensions[0]):
			raise Exception("Cannot remove object: invalid position")
		if (y < 0) or (y >= self.dimensions[1]):
			raise Exception("Cannot remove object: invalid position")
		index = self.bitmap.getpixel((x,y))
		if index == -1:
			raise Exception("Cannot remove object: tile is empty")
		data = self.heap[index]
		if data[0] != (x,y):
			raise Exception("Sanity check failed, mismatch of heap and bitmap position")
		data[1].remove(object)
		
		if data[1] != []:
			return
		self.bitmap.putpixel((x,y),-1)
		if len(self.heap)-1 == index:
			self.heap = self.heap[:-1]
		else:
			relocated = self.heap[-1]
			self.heap[index] = relocated
			self.bitmap.putpixel(relocated[0],index)
			self.heap = self.heap[:-1]
