"""
Module: citygen
Purpose: Automatic generation of city map
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import Image
import ImageDraw
import random
import config

### Class CityMap ###
class CityMap(object):
	# Item types (in the 'Red' pixel)
	(MAP_ITEM_NONE,
	 MAP_ITEM_ROAD,
	 MAP_ITEM_BUILDING,
	 MAP_ITEM_BUILDING_EXTENSION,
	 MAP_ITEM_PARK) = range(5)
	 
	# Road types (in the 'Green' pixel)
	(ROAD_TYPE_NONE,
	 ROAD_TYPE_STRAIGHT,
	 ROAD_TYPE_TURN,
	 ROAD_TYPE_TEE,
	 ROAD_TYPE_CROSS,
	 ROAD_TYPE_TERMINAL) = range(6)
	 
	# Road rotation (in the 'Blue' pixel)
	(ROAD_DIRECTION_UP,
	 ROAD_DIRECTION_LEFT,
	 ROAD_DIRECTION_DOWN,
	 ROAD_DIRECTION_RIGHT) = range(4)
	
	# Color mappings for debugging
	COLOR_MAPPINGS = {
		MAP_ITEM_NONE:		(255, 255, 255),	# White
		MAP_ITEM_ROAD:		(0, 0, 0),			# Black
		MAP_ITEM_BUILDING:	(0, 0, 255),		# Blue
		MAP_ITEM_BUILDING_EXTENSION:	(80, 80, 255),		# lighter Blue
		MAP_ITEM_PARK:		(0, 255, 0),		# Green
	}

	def __init__(self, width, height, roadDensityRange = (4, 15)):
		self.dimensions = (width, height)
		self.roadDensity= roadDensityRange
		self.bitmap 	= Image.new('RGB', self.dimensions, self.MAP_ITEM_NONE)
	
	def getHeight(self):
		return self.dimensions[1]
	
	def getWidth(self):
		return self.dimensions[0]
		
	def getSquare(self, x, y):
		if (x < 0) or (x >= self.dimensions[0]):
			return None
		if (y < 0) or (y >= self.dimensions[1]):
			return None
		return self.bitmap.getpixel((x,y))
		
	def getSquareType(self, x, y):
		if (x < 0) or (x >= self.dimensions[0]):
			return self.MAP_ITEM_NONE
		if (y < 0) or (y >= self.dimensions[1]):
			return self.MAP_ITEM_NONE
	
		return self.bitmap.getpixel((x,y))[0]
		
	def setSquareParam(self, x, y, value):
		squareType, param1, param2 = self.getSquare(x, y)
		self.bitmap.putpixel((x, y), (squareType, param1, value))
	
	def __isFreeSquare(self, x, y):
		return self.bitmap.getpixel((x,y))[0] == self.MAP_ITEM_NONE
		
	def __forEachSquare(self, function, extraArgs = ()):
		for y in range(self.dimensions[1]):
			for x in range(self.dimensions[0]):
				function((x,y), *extraArgs)
	
	# Map Generation routines
	def __generateRoadPoints(self, min, max):
		densityDelta = self.roadDensity[1] - self.roadDensity[0]
		currentPoint = 0
		points = [max - 1]
		while currentPoint < max - 5:
			points.append(currentPoint)
			currentPoint += random.randrange(*self.roadDensity)
		return points
		
	def __calculateRoadDirection(self, position):
		x, y = position
		if self.getSquareType(x, y) != self.MAP_ITEM_ROAD:
			return

		upIsRoad		= self.getSquareType(x, y - 1) == self.MAP_ITEM_ROAD
		leftIsRoad		= self.getSquareType(x - 1, y) == self.MAP_ITEM_ROAD
		downIsRoad		= self.getSquareType(x, y + 1) == self.MAP_ITEM_ROAD
		rightIsRoad		= self.getSquareType(x + 1, y) == self.MAP_ITEM_ROAD
		neighbours 		= [upIsRoad, leftIsRoad, downIsRoad, rightIsRoad]
		directions		= [self.ROAD_DIRECTION_UP,
						   self.ROAD_DIRECTION_LEFT,
						   self.ROAD_DIRECTION_DOWN,
						   self.ROAD_DIRECTION_RIGHT]
		
		roadDirection = self.ROAD_DIRECTION_UP # Default
		# If all neighbours are set, this is a cross road
		if neighbours.count(True) == 4:
			roadType = self.ROAD_TYPE_CROSS
		# If three neighbours are set, this is a T road
		elif neighbours.count(True) == 3:
			roadType = self.ROAD_TYPE_TEE
			roadDirection = directions[(neighbours.index(False) + 2) % len(directions)]
		# Otherwise, it might be straight or turn
		elif neighbours.count(True) == 2:
			roadDirection = directions[neighbours.index(True)]
			if ((upIsRoad and downIsRoad) or
				(leftIsRoad and rightIsRoad)):
				roadType = self.ROAD_TYPE_STRAIGHT
			else:
				roadType = self.ROAD_TYPE_TURN
				# Since we specify direction clock-wise, the only case we don't get it correct
				# is when the turn is right-up
				if rightIsRoad and upIsRoad:
					roadDirection = self.ROAD_DIRECTION_RIGHT
		else:
			assert True in neighbours
			# This is a terminal road
			roadDirection = directions[neighbours.index(True)]
			roadType = self.ROAD_TYPE_TERMINAL
			
		# Update road type & direction
		self.bitmap.putpixel(position, (self.MAP_ITEM_ROAD,
										roadType,
										roadDirection))
		
	def __generateRoads(self):
		# Step 1 - generate the horizontals & verticals, according to road density range
		horizontals = self.__generateRoadPoints(0, self.dimensions[0])
		verticals   = self.__generateRoadPoints(0, self.dimensions[0])
		# draw all horizontals
		draw = ImageDraw.Draw(self.bitmap)
		for y in horizontals:
			startX, stopX = min(verticals), max(verticals)
			samples = random.sample(verticals, 2)
			if random.random() > 0.5:
				startX = min(samples)
			if random.random() > 0.5:
				stopX = max(samples)
			draw.line((startX, y, stopX, y), self.MAP_ITEM_ROAD)
		
		# draw all verticals
		for x in verticals:
			startY, stopY = min(horizontals), max(horizontals)
			samples = random.sample(horizontals, 2)
			if random.random() > 0.5:
				startY = min(samples)
			if random.random() > 0.5:
				stopY = max(samples)
			draw.line((x, startY, x, stopY), self.MAP_ITEM_ROAD)
		
		# Calculate road directions
		self.__forEachSquare(self.__calculateRoadDirection)
	
	def __putBuilding(self, position):
		x, y = position
		if not self.__isFreeSquare(x, y):
			return

		# Building coverage is about 30% for now
		if random.random() > 0.3:
			return
		
		# Select a random building shape
		buildingIndex = random.randrange(len(config.buildings))
		buildingShape = config.buildings[buildingIndex][0]
		
		# Boundary checking
		if x + max([_x for _x,_y in buildingShape]) >= self.dimensions[0]:
			return False
		if y + max([_y for _x,_y in buildingShape]) >= self.dimensions[1]:
			return False
		
		# Validate that we can place the building
		for deltaX, deltaY in buildingShape:
			if not self.__isFreeSquare(x + deltaX, y + deltaY):
				return False
		
		# Now place it
		for deltaX, deltaY in buildingShape:
			self.bitmap.putpixel((x + deltaX, y + deltaY), 
								 (self.MAP_ITEM_BUILDING_EXTENSION,
								  deltaX,
								  deltaY))
	
		# Mark the top-left square
		self.bitmap.putpixel((x, y), 
							 (self.MAP_ITEM_BUILDING, buildingIndex, 0))
							 
		return True
		
	def __putPark(self, position):
		x, y = position
		if self.__isFreeSquare(x, y):
			# Just put park on every free tile
			self.bitmap.putpixel((x,y), (self.MAP_ITEM_PARK, 0, 0))

	def generate(self, withRoads = True):
		if withRoads:
			self.__generateRoads()
		self.__forEachSquare(self.__putBuilding)
		self.__forEachSquare(self.__putPark)
		
	def saveInternalBitmap(self, fileName):
		self.bitmap.save(fileName)

	def loadInternalBitmap(self, fileName):
		self.bitmap = Image.open(fileName)
		self.dimensions = list(self.bitmap.size)

	def loadBitmap(self, fileName):
		newBitmap	= Image.open(fileName)
		self.dimensions = list(newBitmap.size)
		self.bitmap = Image.new('RGB', self.dimensions, self.MAP_ITEM_NONE)
		reverseColorMap = {}
		for item, value in self.COLOR_MAPPINGS.items():
			reverseColorMap[value] = item
		for y in range(self.dimensions[1]):
			for x in range(self.dimensions[0]):
				pixelValue = newBitmap.getpixel((x,y))
				if pixelValue in reverseColorMap:
					self.bitmap.putpixel((x,y), reverseColorMap[pixelValue])
					
		# Fix road directions & types
		self.__forEachSquare(self.__calculateRoadDirection)

	def getColoredBitmap(self, scale):
		coloredBitmap = Image.new('RGB', 
								  self.dimensions, 
								  self.COLOR_MAPPINGS[self.MAP_ITEM_NONE])
								  
		for y in range(self.dimensions[1]):
			for x in range(self.dimensions[0]):
				squareType, param1, param2 = self.getSquare(x,y)
				coloredBitmap.putpixel((x, y),
									   self.COLOR_MAPPINGS[squareType])
									
		if scale == 1:
			return coloredBitmap
		return coloredBitmap.resize((self.dimensions[0] * scale, self.dimensions[1] * scale))

### Test Code ###
if __name__ == "__main__":
	testMap = CityMap(25, 25)
	testMap.generate()
	testMap.getColoredBitmap(4).show()
