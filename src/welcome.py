"""
Module: welcome
Purpose: Defines the 'Welcome' screen
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import sys
import pygame
import config
from level import Level
from tiledobjectsmanager import TiledObjectsManager
import citygen
import gametime
import menu
import person
import scenelevel
import sound

from pgu import gui

#WelcomeLevel = scenelevel.SceneLevel
### Class WelcomeLevel ###
class WelcomeLevel(scenelevel.SceneLevel):
	def __init__(self):
		super(WelcomeLevel, self).__init__()
		self.createGui()
		self.resetGuiSurface()
		self.map_size = (20, 20)
	
	def start(self):
		self.bgMusic = sound.SoundEffect.getSoundEffect(r'../media/sound/welcome.ogg')

		# Load premastered map
		self.map = citygen.CityMap(*self.map_size)
		self.map.loadInternalBitmap('../media/welcome.png')
		self.map_size = (self.map.getWidth(), self.map.getHeight())
		self.look_at = (self.map_size[0] / 2, self.map_size[1] / 2)

		super(WelcomeLevel, self).start(True)
		
		# Put special welcome tiles
		self.map.setSquareParam(5, 4, config.WELCOME_ELEMENT_LEFT * 4 + 1)
		self.map.setSquareParam(6, 4, config.WELCOME_ELEMENT_RIGHT * 4 + 1)

		self.bgMusic.play()
		
	def createGui(self):
		self.gui = gui.App(theme=self.guiTheme)
		
		main = gui.Container(width=800, height=600) # XXX

		self.labelColor = [255,255,255]
		self.labelGoingWhite = False
		doc = gui.Document(width=640)
		self.pressAnyKeyLabel = gui.Button("Press any key to start...", color=self.labelColor)
		self.pressAnyKeyLabel.connect(gui.CLICK, self.handle_key_down, None)

		
		doc.add(self.pressAnyKeyLabel, align=0)
		main.add(doc, 0, 500)

		self.gui.init(main)
		
	def end_level(self):
		self.bgMusic.stop()
		super(WelcomeLevel, self).end_level()
		
	def get_next_level(self):
		return menu.MenuLevel.getInstance()

	def handle_key_down(self, key):
		self.end_level()
