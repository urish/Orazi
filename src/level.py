"""
Module: level
Purpose: Base class for all game screens
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import os
import sys

import pygame
from OpenGL import GL
from pgu import gui, html

import gametime
import config

class Level(object):
	"""
	The levels are *init*ialized when the game starts,
	but *start*ed only when the level starts
	"""
	(LEVEL_STATE_INIT,
	 LEVEL_STATE_ACTIVE,
	 LEVEL_STATE_PAUSED,
	 LEVEL_STATE_DONE) = range(4)
	
	def __init__(self):
		self.level_state = self.LEVEL_STATE_INIT
		self.texture = None
		screenSurface = pygame.display.get_surface()
		self.screenSize = (640, 480)
		if screenSurface:
			self.screenSize = screenSurface.get_size()
		self.intro_screen = None
		self.gui = None
		self.guiTheme = gui.theme.Theme(os.path.abspath('../media/theme'))
		self.ended = False
		self.has_won = False
		self.lazyGui = False
		self.__redrawGui = False
	
	def is_active(self):
		return self.level_state == self.LEVEL_STATE_ACTIVE
	
	def start(self):
		if self.gui:
			GL.glClearColor(1,1,1,0)
		self.level_state = self.LEVEL_STATE_ACTIVE
	
	#also moves the objects on the screen
	def draw(self, frameDelta):
		GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
		
	def resetGuiSurface(self):
		# Taken from lamina 0.1.2
		powerOfTwo = 64
		while powerOfTwo < max(*self.screenSize):
			powerOfTwo *= 2
		raw = pygame.Surface((powerOfTwo, powerOfTwo), pygame.SRCALPHA, 32)
		self.__surfaceTotal = raw.convert_alpha()
		self.__usable = (1.0 * self.screenSize[0] / powerOfTwo, 
						 1.0 * self.screenSize[1] / powerOfTwo)
		self.__guiSurface = self.__surfaceTotal.subsurface(0, powerOfTwo-self.screenSize[1], self.screenSize[0], self.screenSize[1])
	
	def loadGuiTexture(self):
		"""Load surface into texture object. Return texture object. """
		# Based on code from lamina 0.1.2
		
		if not self.texture:
			self.texture = GL.glGenTextures(1)
		
		GL.glEnable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture)
		width, height = self.__surfaceTotal.get_size()
		textureData = pygame.image.tostring(self.__surfaceTotal, "RGBA", 1)
		
		GL.glTexImage2D(GL.GL_TEXTURE_2D, 
						 0,
						 GL.GL_RGBA,
						 width,
						 height,
						 0,
						 GL.GL_RGBA,
						 GL.GL_UNSIGNED_BYTE,
						 textureData)
		GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST)
		GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST)
		GL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_REPLACE)
		GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
		GL.glDisable(GL.GL_TEXTURE_2D)
	
	def redrawGui(self):
		self.__redrawGui = True
	
	def end_level(self):
		self.level_state = self.LEVEL_STATE_DONE
	
	def pause_level(self):
		self.level_state = self.LEVEL_STATE_PAUSED
		
	def get_intro(self):
		return self.intro_screen
	
	def win(self):
		if self.ended:
			return
		self.ended = True
		self.has_won = True
		if self.gui:
			screenWidth, screenHeight = 800, 600
			guiContainer = gui.Container(width=screenWidth, height=screenHeight)
			guiContainer.add(gui.Image(config.LEVEL_OVERLAY_WIN), 0, 0)
			label = gui.Label('You have reached level %d' % (self.level_number + 1), color = (255, 220, 160))
			guiContainer.add(label,	(screenWidth - label.style.width) / 2, 400)
			self.gui.init(guiContainer)	
			self.redrawGui()
		if config.print_debug:
			print "You win the level"
	
	def lose(self):
		if self.ended:
			return
		self.ended = True
		if self.gui:
			self.gui.init(gui.Image(config.LEVEL_OVERLAY_LOSE))
			self.redrawGui()		
		if config.print_debug:
			print "You lose the level"

	def show_html_dialog(self, title, htmlFile):
		mainTable = gui.Table(width=200)
		dlg = gui.Dialog(gui.Label(title), mainTable)
		mainTable.tr()
		
		# Add HTML viewer
		doc = html.HTML(file(htmlFile).read(), width=300)
		view = gui.ScrollArea(doc, 320, 350, hscrollbar=False)
		mainTable.td(view)
		
		# Add the close button
		mainTable.tr()
		b = gui.Button("Close", width=80)
		b.connect(gui.CLICK, dlg.close, None)
		mainTable.td(b)
		
		dlg.open()
			
	def draw_overlay(self):
		if self.gui:
			self.draw_guiOverlay()
	
	def draw_guiOverlay(self):
		if (not self.texture) or (not self.lazyGui) or (self.__redrawGui):
			self.gui.paint(self.__guiSurface)
			self.loadGuiTexture()
			self.__redrawGui = False
		
		GL.glDisable(GL.GL_LIGHTING)
		GL.glColor3f(1,1,1)
		
		GL.glEnable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture)

		GL.glDisable(GL.GL_DEPTH_TEST)
		
		GL.glMatrixMode(GL.GL_TEXTURE)
		GL.glPushMatrix()
		GL.glLoadIdentity()
		GL.glScalef(self.__usable[0],self.__usable[1],1)
		
		#draw
		GL.glEnable(GL.GL_BLEND)
		GL.glBegin(GL.GL_QUADS)
		for coord in [(0,0),(0,1),(1,1),(1,0)]:
			GL.glTexCoord2f(*coord)
			GL.glVertex2f(*coord)
		GL.glEnd()
		
		#restore the matrices
		#GL.glMatrixMode(GL.GL_TEXTURE)
		GL.glPopMatrix()
		GL.glMatrixMode(GL.GL_MODELVIEW)

		GL.glEnable(GL.GL_DEPTH_TEST)
		
		# unbind the texture
		GL.glDisable(GL.GL_BLEND)
		GL.glDisable(GL.GL_TEXTURE_2D)
	
	def handle_video_expose(self, fullDraw = True):
		gametime.updateFrame()
		if fullDraw:
			self.draw(min(1.0,gametime.getFrameDelta()))
		GL.glMatrixMode(GL.GL_PROJECTION)
		GL.glLoadIdentity()
		GL.glMatrixMode(GL.GL_MODELVIEW)
		GL.glLoadIdentity()
		GL.glScalef(2,2,1)
		GL.glTranslatef(-0.5,-0.5, 0)
		self.draw_overlay()
		GL.glFlush()
		pygame.display.flip()
		return True
	
	def get_next_level(self):
		return None

	def handle_key_down(self, key):
		if key == pygame.K_ESCAPE:
			self.end_level()
	
	def handle_mouse_motion(self, pos, rel, buttons):
		#buttons[0] == 1 <-> left button clicked.
		#[1] - middle, [2] - right
		#[4] - scroll up, [5] - scroll down
		pass
	
	def handle_mouse_up(self, pos, button):
		pass
	
	def handle_mouse_down(self, pos, button):
		pass
	
	def handle_event(self,event):
		if event.type == pygame.QUIT:
			sys.exit(0)
		elif event.type == pygame.VIDEOEXPOSE:
			self.handle_video_expose()
		elif event.type == pygame.ACTIVEEVENT:
			pass #gain, state
		elif event.type == pygame.KEYDOWN:
			self.handle_key_down(event.key) #unicode, key, mod
		elif event.type == pygame.KEYUP:
			pass #key, mod
		elif event.type == pygame.MOUSEMOTION:
			self.handle_mouse_motion(event.pos, event.rel, event.buttons)
		elif event.type == pygame.MOUSEBUTTONUP:
			self.handle_mouse_up(event.pos, event.button)
		elif event.type == pygame.MOUSEBUTTONDOWN:
			self.handle_mouse_down(event.pos, event.button)
		elif event.type == pygame.VIDEORESIZE:
			raise Exception("Error! The window should not be resized")#      size, w, h
		else:
			pass #not interesting
		
		# Pass event to GUI layer, if anys
		if self.gui:
			self.gui.event(event)
	
	def update_gui_content(self):
		pass
