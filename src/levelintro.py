"""
Module: levelinfo
Purpose: Displays the 'level intro', a screen with instructions shown before
		 each level.
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import os
import sys
import pygame
import menu

from pgu import gui, html
from level import Level

### Class LevelIntro ###
class LevelIntro(Level):
	def __init__(self, levelName, levelScreen, testingMode = False, isHelp = False):
		super(LevelIntro, self).__init__()
		self.__testingMode  = True
		self.levelName 		= levelName
		self.levelScreen	= levelScreen
		self.isHelp			= (levelName == 'help')
		self.createGui()
		self.resetGuiSurface()
		self.texture = None

	def createGui(self):
		self.gui = gui.App(theme=self.guiTheme)
		
		main = gui.Container(width=640, height=480, background=(0,0,0))
		
		bg = gui.Image("../media/OraziBG.png")
		main.add(bg, 0, 0)
		
		if self.isHelp:
			htmlFile = '../media/text/help.html'
			resumeButtonText = 'Resume Game'
		else:
			htmlFile = '../media/text/intro/%s.html' % self.levelName
			resumeButtonText = 'Play !'
		self.menuHtml = html.HTML(file(htmlFile).read(), width=450, height=200)
		
		startButton = gui.Button(resumeButtonText, width=80)
		startButton.connect(gui.CLICK, self.handle_key_down, None)

		table = gui.Table()
		table.tr()
		table.td(self.menuHtml)
		table.tr()
		table.td(startButton)
		
		main.add(table, 80, 160)

		self.gui.init(main)
		
	def get_next_level(self):
		return self.levelScreen

	def handle_key_down(self, key):
		if key == pygame.K_ESCAPE:
			self.levelScreen = menu.MenuLevel.getInstance()
		self.end_level()
		
	def runTest(self):
		self.gui.run()

### Testing Code ###
if __name__ == '__main__':
	level = LevelIntro('findorazi', None, True)
	level.runTest()
