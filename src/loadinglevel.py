"""
Module: loadinglevel
Purpose: Defines the 'Loading Level' screen
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import os
import sys

from pgu import gui
import config
from level import Level

### Class LoadingLevel ###
class LoadingLevel(Level):
	def __init__(self, testingMode = False):
		super(LoadingLevel, self).__init__()
		self.__testingMode = True
		self.createGui()
		self.resetGuiSurface()
		self.texture = None

	def createGui(self):
		self.gui = gui.App(theme=self.guiTheme)
		
		main = gui.Container(width=500, height=400)

		bg = gui.Image("../media/OraziBG.png")
		main.add(bg, 0, 0)
		overlay = gui.Image("../media/LoadingLevelOverlay.png")
		main.add(overlay, 0, 0)
		
		self.gui.init(main)
	
	def runTest(self):
		self.gui.run()

### Testing Code ###
if __name__ == '__main__':
	level = LoadingLevel(True)
	level.runTest()
