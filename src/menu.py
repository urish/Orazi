"""
Module: menu
Purpose: Defines the main menu screen
Author: Uri Shaked <uri@keves.org>
Revision: $Id$
"""

### Imports ###
import os
import sys
import config

import pygame
from pgu import gui, html

from level import Level

### Global Variables ###
g_menuInstance = None

### Class MenuLevel ###
class MenuLevel(Level):
	def __init__(self, testingMode = False):
		super(MenuLevel, self).__init__()
		self.__testingMode = True
		self.createGui()
		self.resetGuiSurface()
		self.__nextLevel = None
		self.__resumeLevel = None
	
	@staticmethod
	def getInstance():
		global g_menuInstance
		if not g_menuInstance:
			g_menuInstance = MenuLevel()
			
		g_menuInstance.setResumeMode(False)
		return g_menuInstance

	def get_next_level(self):
		self.pause_level()
		return self.__nextLevel
		
	def createGui(self):
		if self.__testingMode:
			self.gui = gui.Desktop(background=(0,0,0), theme=self.guiTheme)
		else:
			self.gui = gui.App(theme=self.guiTheme)

		main = gui.Container(width=500, height=400)

		bg = gui.Image("../media/OraziBG.png")
		main.add(bg, 0, 0)
		overlay = gui.Image("../media/MenuOverlay.png")
		main.add(overlay, 0, 0)

		self.menuHtml = html.HTML(file('../media/text/menu.html').read(), width=200, height=200)
		main.add(self.menuHtml, 70, 144)
		
		# Connect the various GUI buttons
		self.menuHtml['playButton'].connect(gui.CLICK, self.commandPlay, None)
		self.menuHtml['resumeButton'].connect(gui.CLICK, self.commandResume, None)
		self.menuHtml['aboutButton'].connect(gui.CLICK, self.commandAbout, None)
		self.menuHtml['exitButton'].connect(gui.CLICK, self.commandExit, None)
		
		# Resume is disable by default
		self.menuHtml['resumeButton'].disabled = True
		self.menuHtml['resumeButton'].blur()
		self.menuHtml['resumeButton'].chsize()
		
		# Populate the level list
		levelList = self.menuHtml['levelList']
		for levelName, levelClass in config.GAME_LEVELS:
			levelList.add(levelName, value=levelClass)
		
		# Select the first level
		levelList.group.value = config.GAME_LEVELS[0][1]
		levelList.value = levelList.group.value
		levelList.resize()
		levelList.repaint()

		self.gui.init(main)
		
	def setResumeMode(self, resumeLevel):
		self.__resumeLevel = resumeLevel
		self.__nextLevel   = None

		# Update the GUI according to the chosen resume mode
		self.menuHtml['resumeButton'].disabled = True
		if self.__resumeLevel:
			self.menuHtml['resumeButton'].disabled = False
		self.menuHtml['resumeButton'].chsize()
		self.menuHtml['resumeButton'].repaint()
	
	def commandPlay(self, arg):
		if self.menuHtml['levelList'].value:
			self.__nextLevel = self.menuHtml['levelList'].value()
			self.pause_level()

	def commandResume(self, arg):
		if self.__resumeLevel:
			self.__nextLevel = self.__resumeLevel
			self.pause_level()
		
	def commandAbout(self, arg):
		self.show_html_dialog('About', '../media/text/about.html')
		
	def commandExit(self, arg):
		self.end_level()
		if self.__testingMode:
			raise SystemExit(1)

	def handle_key_down(self, key):
		if key == pygame.K_RETURN:
			self.commandPlay(None)
			return
		return super(MenuLevel, self).handle_key_down(key)
			
	def runTest(self):
		self.gui.run()

### Testing Code ###
if __name__ == '__main__':
	level = MenuLevel(True)
	level.runTest()
