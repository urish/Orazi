"""
Module: textures
Purpose: Texture handling
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import pygame
from OpenGL import GL,GLU

### Class Texture ####
class Texture:
	def __init__(self, filename):
		self.gl_texture = GL.glGenTextures(1)
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.gl_texture)
		#load the file into opengl
		tex_image = pygame.image.load(filename)
		tex_size = tex_image.get_size()
		string = pygame.image.tostring(tex_image,"RGBA")
		if True:
			GLU.gluBuild2DMipmaps(GL.GL_TEXTURE_2D, GL.GL_RGBA,
				tex_size[0],tex_size[1],
				GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,string)
		else:
			GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA,
				tex_size[0],tex_size[1],0,
				GL.GL_RGBA,GL.GL_UNSIGNED_BYTE,string)
		
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
		GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
	
	def bind(self):
		GL.glEnable(GL.GL_TEXTURE_2D)
		GL.glBindTexture(GL.GL_TEXTURE_2D, self.gl_texture)

### Global Functions ###
def unbind():
	GL.glDisable(GL.GL_TEXTURE_2D)
	GL.glBindTexture(GL.GL_TEXTURE_2D, 0)

texture_library = {}
def getTexture(filename):
	global texture_library 
	if filename not in texture_library.keys():
		texture_library[filename] = Texture(filename)
	return texture_library[filename]
