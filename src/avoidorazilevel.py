"""
Module: AvoidOraziLevel
Purpose: Defines the game level of 'Avoid Orazi'
Author: Nir Hershko
Revision: $Id: findorazilevel.py 1021 2006-09-09 21:43:11Z nirh $
"""

### Imports ###
import pygame
from pgu import gui, html

import gametime
from scenelevel import SceneLevel
import person
import config
import levelintro
import sound
import random

### Class AvoidOraziLevel ###
class AvoidOraziLevel(SceneLevel):
	def __init__(self):
		super(AvoidOraziLevel, self).__init__()
		self.intro_screen = levelintro.LevelIntro('avoidorazi', self)
		
		self.level_number = 1
		self.time_limit = config.AVOID_ORAZI_TIME_LIMIT
		self.signs_in_inventory = 0
		self.map_size = (50,50) #run over scene level config
		
		# Create the GUI overlay
		self.createGui()
		self.resetGuiSurface()
		self.update_gui_content()
		
		self.lazyGui = True

	def createGui(self):
		self.gui = gui.App(theme=self.guiTheme)
		
		main = gui.Container(width=800, height=600)
		
		overlay = gui.Image("../media/LevelOverlay2.png")
		
		overlayX, overlayY = 0, main.rect[3] - 40 #XXX
		main.add(overlay, overlayX, overlayY)
		
		self.orazimLeftWidget = gui.Label('001')
		main.add(self.orazimLeftWidget, overlayX + 45, overlayY + 12)
		self.timeLeftWidget = gui.Label('00:00')
		main.add(self.timeLeftWidget, overlayX + 100, overlayY + 12)
		
		self.gui.init(main)
	
	def updateOverlay(self, secondsLeft, orazimCount):
		self.orazimLeftWidget.value = '%02d' % orazimCount
		self.timeLeftWidget.value = '%02d:%02d' % (secondsLeft / 60, secondsLeft % 60)
		self.redrawGui()
	
	def start(self):
		self.bgMusic = sound.SoundEffect.getSoundEffect(r'..\media\sound\march.ogg')
		SceneLevel.start(self)
		
		self.player = person.create_random_person(person.PlayerPerson)
		
		for x in range(1):
			self.orazim.append(person.create_random_person(person.Orazi))
		
		#config.test_object = person.create_random_person()#Test
		self.bgMusic.playInfinite()
	
	def draw(self, frameDelta):
		self.look_at = self.player.get_position()
		
		SceneLevel.draw(self, frameDelta)
		
		if random.random()*60 > 60-frameDelta*self.level_number:
			self.orazim.append(person.create_random_person(person.Orazi))
			self.update_gui_content()
		
		if self.ended:
			return
		
		for orazi in self.orazim:
			huyals = self.tom.get_objects(*map(int,orazi.get_position()))
			for huyal in huyals:
				huyal.color = (0,0.3,0)
		
		objects = self.tom.get_objects(*map(int,self.look_at))
		for o in objects:
			if isinstance(o,person.Orazi):
				self.lose()
		
		if gametime.getLevelRunningTime() > self.time_limit:
			self.win()
	
	def generate_harder_level(self):
		new_level = self.__class__()
		new_level.map_size = (self.map_size[0]-10,self.map_size[1]-10)
		if self.map_size[0] < config.MINIMUM_REASONABLE_MAP_SIZE:
			self.map_size[0] = config.MINIMUM_REASONABLE_MAP_SIZE
		if self.map_size[1] < config.MINIMUM_REASONABLE_MAP_SIZE:
			self.map_size[1] = config.MINIMUM_REASONABLE_MAP_SIZE
		new_level.level_number = self.level_number + 1
		if self.level_number:
			new_level.intro_screen = levelintro.LevelIntro('avoidorazi2', new_level)
		return new_level
	
	def update_gui_content(self):
		if self.ended:
			return
		timeleft = self.time_limit - gametime.getLevelRunningTime()
		self.updateOverlay(timeleft, len(self.orazim))
