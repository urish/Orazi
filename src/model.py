"""
Module: model
Purpose: Classes for loading & drawing of 3D models & animations.
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
from OpenGL import GL

import textures
import config

### Class _Meterial ###
class _Material:
	pass

### Class _MeterialInfo ###
class _MaterialInfo:
	
	def __init__(self, category, filename):
		self.materials = {}
		
		content = map(str.strip,file("../model/%s/%s" % (category,filename),"r").readlines())
		current_material = None
		for line in content:
			if line == "" or line[0] == "#":
				continue
			elif line.startswith("newmtl "):
				current_material = _Material()
				self.materials[line[7:]] = current_material
			elif line.startswith("Ka "):
				current_material.Ka = map(float,line[3:].strip().split(" "))
			elif line.startswith("Kd "):
				current_material.Kd = map(float,line[3:].strip().split(" "))
			elif line.startswith("Ks "):
				current_material.Ks = map(float,line[3:].strip().split(" "))
			elif line.startswith("map_Kd "):
				tex_name = line[7:].strip()
				if tex_name != "Undefined":
					tex_filename = "../model/%s/%s" % (category, tex_name)
					current_material.map_Kd = textures.getTexture(tex_filename)
			elif line.startswith("d "):
				current_material.d = float(line[2:].strip())
			elif line.startswith("Ns "):
				current_material.Ns = float(line[3:].strip())
			elif line.startswith("illum "):
				current_material.illum = float(line[6:].strip())
			else:
				raise Exception("Unknown line in mtl file")

	def usemtl(self, matstring):
		material = self.materials[matstring]
		if hasattr(material,"Ka"):
			GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,material.Ka)
		if hasattr(material,"Kd"):
			GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,material.Kd)
		if hasattr(material,"Ks"):
			GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_SPECULAR,material.Ks)
		
		if hasattr(material,"map_Kd"):
			material.map_Kd.bind()
		else:
			textures.unbind()

### Class Model ###
class Model:
	def __init__(self, category, filename):
		def parse_vector_float(string):
			return 
		
		self.vertices = []
		self.tex_coords = []
		self.normals = []
		self.faces = []
		self.material = None
		
		cubify = False
		
		if cubify:
			max_vertex_x = -10000
			min_vertex_x = +10000
			max_vertex_y = -10000
			min_vertex_y = +10000
			max_vertex_z = -10000
			min_vertex_z = +10000
		
		full_filename = "../model/%s/%s" % (category, filename)
		if config.print_debug:
			print "Loading \"%s\".." % full_filename
		content = map(str.strip,file(full_filename,"r").readlines())
		is_triangulated = True
		for line in content:
			if line.startswith("mtllib"):
				self.material = _MaterialInfo(category, line[6:].strip())
			elif line.startswith("f "):
				points = line[2:].split(" ")
				if len(points) > 3:
					is_triangulated = False
			elif line.startswith("v "):
				vertex = map(float,line[2:].strip().split(" "))
				if cubify:
					if vertex[0] > max_vertex_x:
						max_vertex_x = vertex[0]
					if vertex[0] < min_vertex_x:
						min_vertex_x = vertex[0]
					if vertex[1] > max_vertex_y:
						max_vertex_y = vertex[1]
					if vertex[1] < min_vertex_y:
						min_vertex_y = vertex[1]
					if vertex[2] > max_vertex_z:
						max_vertex_z = vertex[2]
					if vertex[2] < min_vertex_z:
						min_vertex_z = vertex[2]
				self.vertices.append( map(float,line[2:].strip().split(" ")) )
			elif line.startswith("vt "):
				self.tex_coords.append( map(float,line[3:].strip().split(" ")) )
			elif line.startswith("vn "):
				self.normals.append( map(float,line[3:].strip().split(" ")) )
		if self.material == None:
			raise Exception("No material file specified in obj file")
		
		self.display_list = GL.glGenLists(1)
		GL.glNewList(self.display_list,GL.GL_COMPILE)
		
		if cubify:
			GL.glPushMatrix()
			xScale = 0.8/(max_vertex_x - min_vertex_x)
			yScale = 0.8/(max_vertex_y - min_vertex_y)
			zScale = 0.8/(max_vertex_z - min_vertex_z)
			GL.glScalef(xScale,yScale,zScale)
			GL.glTranslatef(min_vertex_x,min_vertex_y,min_vertex_z)
		
		#drawing code
		GL.glEnable(GL.GL_LIGHTING)
		if is_triangulated:
			GL.glBegin(GL.GL_TRIANGLES)
		for line in content:
			if line == "" or line[0] == "#":
				continue
			elif line.startswith("mtllib"):
				pass #done already
			elif line == "g" or line.startswith("g ") or line.startswith("s "):
				pass #oh well
			elif line.startswith("usemtl "):			
				if is_triangulated:
					GL.glEnd()
				self.material.usemtl(line[7:])
				if is_triangulated:
					GL.glBegin(GL.GL_TRIANGLES)
			elif line.startswith("v ") or line.startswith("vt ") or line.startswith("vn "):
				pass
			elif line.startswith("f "):
				points = line[2:].split(" ")
				if not is_triangulated:
					GL.glBegin(GL.GL_POLYGON)
				#points.reverse()
				for point in points:
					info = map(int,point.split("/"))
					#print "info:", info
					texcoord = self.tex_coords[info[1]-1]
					GL.glTexCoord2d(texcoord[0],texcoord[1])
					#print "texture:", (texcoord[0],texcoord[1])
					normal = self.normals[info[2]-1]
					GL.glNormal3d(normal[0],normal[1],normal[2])
					#print "normal:", (normal[0],normal[1],normal[2])
					vertex = self.vertices[info[0]-1]
					GL.glVertex3d(vertex[0],vertex[1],vertex[2])
					#print "vertex:", (vertex[0],vertex[1],vertex[2])
				if not is_triangulated:
					GL.glEnd()
				#raise ""
			else:
				print line
				raise Exception("Unknown line in obj file")
		if is_triangulated:
			GL.glEnd()
		
		if cubify:
			GL.glPopMatrix()
		
		GL.glEndList()
	
	def draw(self):
		GL.glPushMatrix()
		GL.glRotate(90,0,0,1)
		GL.glCallList(self.display_list)
		GL.glPopMatrix()

model_library = {}
def getModel(category, filename):
	global model_library 
	if (category, filename) not in model_library.keys():
		model_library[(category, filename)] = Model(category, filename)
	return model_library[(category, filename)]

### Class Animation ###
class Animation:
	#usage: person_anim = Animation("..\models\person%d",32,0.04+random.random()*0.01)
	def __init__(self, model_filename_format, frame_count, frame_time):
		self.frame_count = frame_count
		self.frame_time = frame_time
		self.frames = []
		for i in range(frame_count):
			self.frames = getModel(model_filename_format % i)
		
		self.fps = 1.0/frame_time
		self.current_frame = 0
	
	def draw(self, timeDelta):
		self.current_frame = (self.current_frame + self.fps*timeDelta) % self.frame_count
		self.frames[self.current_frame].draw()
