"""
Module: movingobject
Purpose: Defines the base class for all moving (non-static) objects in game scene.
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import config

### Class MovingObject ###
class MovingObject(object):
	def __init__(self,position):
		self._position = position
		(x,y) = (int(self._position[0]), int(self._position[1]))
		config.current_level.tom.add_object(x,y,self)
		
	def get_position(self):
		return self._position
	
	def set_position(self, new_position):
		(x,y) = (int(self._position[0]), int(self._position[1]))
		self._position = new_position
		(newx,newy) = (int(self._position[0]), int(self._position[1]))
		if (x,y) != (newx,newy):
			config.current_level.tom.remove_object(x,y,self)
			config.current_level.tom.add_object(newx,newy,self)

	def move(self):
		pass
	
	def draw(self):
		pass
