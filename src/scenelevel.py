"""
Module: scenelevel
Purpose: Defines the 3D SceneLevel base class, from which all game classes derive.
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import math
import random

from OpenGL import GL,GLU,GLUT
import pygame

from level import Level
from tiledobjectsmanager import TiledObjectsManager
import gametime
import model
import menu
import citygen
import config
import person
import levelintro

### Global Functions ###
def _draw_color_square(x,y,sizex,sizey,color):
	if len(color) == 4:
		GL.glColor4f(color[0]/255.0,color[1]/255.0,color[2]/255.0,color[3]/255.0)
	else:
		GL.glColor3f(color[0]/255.0,color[1]/255.0,color[2]/255.0)
	GL.glPushMatrix()
	GL.glTranslatef(x,y,0)
	GL.glScalef(sizex,sizey,1)
	if len(color) == 4:
		GL.glEnable(GL.GL_BLEND)
	GL.glBegin(GL.GL_QUADS)
	GL.glVertex3f(0,0,0)
	GL.glVertex3f(0,+1,0)
	GL.glVertex3f(+1,+1,0)
	GL.glVertex3f(+1,0,0)
	GL.glEnd()
	if len(color) == 4:
		GL.glDisable(GL.GL_BLEND)
	GL.glPopMatrix()

def _draw_building(building_pos,building_model,building_info):
	bsize   = config.buildings[building_model][2]
	bheight = config.buildings[building_model][1]
	GL.glDisable(GL.GL_TEXTURE_2D)
	GL.glEnable(GL.GL_LIGHTING)
		
	color = config.RANDOM_COLORS[building_info]
	GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,color)
	GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.1,0.1,0.1))
	GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_SPECULAR,(0.1,0.1,0.1))
	#_draw_color_square(building_pos[0],building_pos[1],bsize[0],bsize[1],(100,100,100))
	#GL.glColor3f(*color)
	GL.glPushMatrix()
	GL.glTranslatef(building_pos[0],building_pos[1],0)
	#GL.glScalef(0.01,0.01,0.01)
	#model.draw()
	GL.glTranslatef(0.1,0.1,0)
	GL.glScalef(bsize[0]-0.2,bsize[1]-0.2,bheight)
	GL.glTranslatef(0.5,0.5,0.5)
	GLUT.glutSolidCube(1)
	GL.glPopMatrix()

def _draw_textured_tile(texture,rotation,position):
	texture.bind()
	GL.glEnable(GL.GL_TEXTURE_2D)
	GL.glPushMatrix()
	GL.glTranslatef(position[0],position[1],0)
	GL.glBegin(GL.GL_QUADS)
	v = [(0,0),(0,1),(1,1),(1,0)]
	for i in range(4):
		GL.glTexCoord3f(v[(i-rotation)%4][0],v[(i-rotation)%4][1],0)
		GL.glVertex3f(v[i][0],v[i][1],0)
	GL.glEnd()
	GL.glPopMatrix()
	GL.glDisable(GL.GL_TEXTURE_2D)

### Class SceneLevel ###
class SceneLevel(Level):
	"""
	This class represents a level scene, including the map (of roads and structures)
	and people hanging around. This class holds together all the main logic of the game.
	"""
	def __init__(self):
		Level.__init__(self)
		self.map = None #until start level
		self.tom = None #TiledObjectsManager
		self.height_angle = config.MINIMUM_HEIGHT_ANGLE
		self.rotation_angle = 0
		self.camera_distance = config.MAXIMUM_CAMERA_DISTANCE
		self.return_to_menu = False
		self.show_help = False
		self.orazim = []
		self.map = None
		self.player = None
		self.signs = {} #position:rotation
		self.look_at = (10,10)
		
		self.map_size = (50,50)
	
	def is_road_at(self,x,y):
		return (self.map.getSquareType(x,y) == citygen.CityMap.MAP_ITEM_ROAD)
	
	def _set_camera(self):
		#up is (0,0,1)
		eyex = self.camera_distance * math.sin(self.height_angle) * math.sin(self.rotation_angle)
		eyey = self.camera_distance * math.sin(self.height_angle) * math.cos(self.rotation_angle)
		eyez = self.camera_distance * math.cos(self.height_angle)
		
		eyex += self.look_at[0]
		eyey += self.look_at[1]
		
		GL.glLoadIdentity()
		GLU.gluLookAt(eyex,eyey,eyez,
					  self.look_at[0],self.look_at[1],0,
					  0,0,1)
	
	def _draw_map(self):
		GL.glDisable(GL.GL_LIGHTING)
		buildings_to_draw = set()
		GL.glColor4f(1,1,1,1)
		minx = int(self.look_at[0])-config.max_tiles_on_screen
		maxx = int(self.look_at[0])+config.max_tiles_on_screen
		miny = int(self.look_at[1])-config.max_tiles_on_screen
		maxy = int(self.look_at[1])+config.max_tiles_on_screen
		for x in range(minx, maxx+1):
			for y in range(miny, maxy+1):
				tile = self.map.getSquare(x,y)
				if tile == None:
					continue
				
				if tile[0] == citygen.CityMap.MAP_ITEM_PARK:
					# Draw a park
					if tile[2] != 0:
						GL.glDepthMask(0)
					_draw_textured_tile(config.park_tiles[0],0,(x,y))
					if tile[2] != 0:
						GL.glDepthMask(1)
						GL.glEnable(GL.GL_BLEND)
						_draw_textured_tile(config.park_elements[(tile[2]-1)/4],(tile[2]-1)%4,(x,y))
						GL.glDisable(GL.GL_BLEND)
				
				elif tile[0] == citygen.CityMap.MAP_ITEM_BUILDING:
					_draw_color_square(x,y,1,1,(200,200,200))
					buildings_to_draw.add(((x,y),tile[1],tile[2]))
				
				elif tile[0] == citygen.CityMap.MAP_ITEM_BUILDING_EXTENSION:
					_draw_color_square(x,y,1,1,(200,200,200))
					bx = x-tile[1]
					by = y-tile[2]
					btile = self.map.getSquare(bx,by)
					if btile[0] != citygen.CityMap.MAP_ITEM_BUILDING:
						raise Exception("Building extension points to non-building")
					buildings_to_draw.add(((bx,by),btile[1],btile[2]))
				
				elif tile[0] == citygen.CityMap.MAP_ITEM_ROAD:
					if tile[1] == citygen.CityMap.ROAD_TYPE_NONE:
						raise Exception("Invalid road type")
					if (x,y) in self.signs:
						GL.glDepthMask(0)
						texture = config.road_tiles[tile[1]]
						_draw_textured_tile(texture,tile[2],(x,y))
						GL.glDepthMask(1)
						GL.glColor4f(1,1,1,0.5)
						GL.glEnable(GL.GL_BLEND)
						_draw_textured_tile(config.sign_tile,self.signs[(x,y)],(x,y))
						GL.glDisable(GL.GL_BLEND)
						GL.glColor4f(1,1,1,1)
					else:
						texture = config.road_tiles[tile[1]]
						_draw_textured_tile(texture,tile[2],(x,y))
				
				elif tile[0] == citygen.CityMap.MAP_ITEM_NONE:
					pass#WTF? that ugly welcome screen..
				
				else:
					raise Exception("Unrecognized tile type")
		
		for building_info in buildings_to_draw:
			_draw_building(*building_info)

	def _draw_people(self,frameDelta):
		#print len(self.tom.heap)
		minx = int(self.look_at[0])-config.max_tiles_on_screen
		maxx = int(self.look_at[0])+config.max_tiles_on_screen
		miny = int(self.look_at[1])-config.max_tiles_on_screen
		maxy = int(self.look_at[1])+config.max_tiles_on_screen
		for x in range(minx, maxx+1):
			for y in range(miny, maxy+1):
				#objects is a copy of the get_objects() result
				objects = self.tom.get_objects(x,y)[:]
				for object in objects:
					object.draw()
					if not self.ended:
						object.move(frameDelta)
		
		#don't forget: orazi is always around
		for o in self.orazim:
			o.draw()
			if not self.ended:
				o.move(frameDelta)
		
		#and move the player whatsoever, though that should not be needed
		o = self.player
		if o != None:
			o.draw()
			if not self.ended:
				o.move(frameDelta)
	
	def start(self, isWelcome = False):
		Level.start(self)
		
		if not self.map:
			self.map = citygen.CityMap(*self.map_size)
			self.map.generate()

		self.tom = TiledObjectsManager(*self.map_size)
		self.orazim = []
			
		for x in range(self.map_size[0]):
			for y in range(self.map_size[1]):
				tile = self.map.getSquare(x,y)
				if tile[0] == citygen.CityMap.MAP_ITEM_PARK:
					park_object = int(random.random()*config.PARK_OBJECT_COUNT)
					park_object = park_object*4 + int(4*random.random())
					park_object = park_object + 1
					if random.random() > config.PARK_OBJECT_CHANCE:
						park_object = 0
					self.map.setSquareParam(x,y,park_object)
				if tile[0] == citygen.CityMap.MAP_ITEM_BUILDING:
					self.map.setSquareParam(x,y,int(len(config.RANDOM_COLORS)*random.random()))
				# Special handling - for welcome screen
				if (tile[0] == citygen.CityMap.MAP_ITEM_ROAD) and isWelcome:
					person.WelcomeOrazi((x + 0.5, y + 0.5))
		
		GL.glClearColor(0,0,0,0)
		GL.glEnable(GL.GL_DEPTH_TEST)

		if not isWelcome:
			for x in range(300):
				person.create_random_person()
	
	def draw(self, frameDelta):
		Level.draw(self, frameDelta)
		
		if self.player and not self.ended:
			self.rotation_angle = -(self.player.direction+90) * math.pi / 180
		
		GL.glMatrixMode(GL.GL_PROJECTION)
		GLU.gluPerspective(30,800.0/600.0,1,300)
		GL.glMatrixMode(GL.GL_MODELVIEW)
		
		self._set_camera()
		
		GL.glEnable(GL.GL_LIGHT0)
		GL.glLightfv(GL.GL_LIGHT0,GL.GL_POSITION,(1,1,1,0))
		GL.glLightfv(GL.GL_LIGHT0,GL.GL_AMBIENT,(0.2,0.2,0.2,1))
		GL.glLightfv(GL.GL_LIGHT0,GL.GL_DIFFUSE,(0.8,0.8,0.8,1))
		GL.glLightfv(GL.GL_LIGHT0,GL.GL_SPECULAR,(1,1,1,1))
		GL.glEnable(GL.GL_LIGHT1)
		GL.glLightfv(GL.GL_LIGHT1,GL.GL_POSITION,(-1,-1,-1,0))
		GL.glLightfv(GL.GL_LIGHT1,GL.GL_AMBIENT,(0.2,0.2,0.2,1))
		GL.glLightfv(GL.GL_LIGHT1,GL.GL_DIFFUSE,(0.8,0.8,0.8,1))
		GL.glLightfv(GL.GL_LIGHT1,GL.GL_SPECULAR,(1,1,1,1))
		
		self._draw_map()
		self._draw_people(frameDelta)
		
		if self.ended:
			self.camera_distance /= math.pow(config.CAMERA_DISTANCE_SPEED,frameDelta)
			if self.camera_distance < config.MINIMUM_CAMERA_DISTANCE:
				self.camera_distance = config.MINIMUM_CAMERA_DISTANCE
			self.rotation_angle += config.CAMERA_ROTATION_SPEED * frameDelta
		
		keys = pygame.key.get_pressed()
		if keys[pygame.K_PAGEUP]:
			self.camera_distance /= math.pow(config.CAMERA_DISTANCE_SPEED,frameDelta)
			if self.camera_distance < config.MINIMUM_CAMERA_DISTANCE:
				self.camera_distance = config.MINIMUM_CAMERA_DISTANCE
		elif keys[pygame.K_PAGEDOWN]:
			self.camera_distance *= math.pow(config.CAMERA_DISTANCE_SPEED,frameDelta)
			if self.camera_distance > config.MAXIMUM_CAMERA_DISTANCE:
				self.camera_distance = config.MAXIMUM_CAMERA_DISTANCE
	
	def show_help_screen(self):
		self.show_help = True
		self.pause_level()
	
	def get_next_level(self):
		if self.return_to_menu:
			menuInstance = menu.MenuLevel.getInstance()
			menuInstance.setResumeMode(self)
			self.return_to_menu = False
			return menuInstance
		if self.show_help:
			self.show_help = False
			return levelintro.LevelIntro('help', self)
		if self.has_won:
			return self.generate_harder_level()
		else:
			menuInstance = menu.MenuLevel.getInstance()
			menuInstance.setResumeMode(self)
			return menuInstance

	def handle_mouse_motion(self, pos, rel, buttons):
		if buttons[2]:
			self.rotation_angle += rel[0]*0.01
			self.rotation_angle = self.rotation_angle % (2*math.pi)
			
			self.height_angle += rel[1]*0.01
			if self.height_angle < 0.001:
				self.height_angle = 0.001
			elif self.height_angle > config.MINIMUM_HEIGHT_ANGLE:
				self.height_angle = config.MINIMUM_HEIGHT_ANGLE
			return True
		return super(SceneLevel, self).handle_mouse_motion(pos, rel, buttons)
	
	def handle_mouse_down(self, pos, button):
		if button == 4:
			self.camera_distance /= config.CAMERA_DISTANCE_SCROLL
			if self.camera_distance < config.MINIMUM_CAMERA_DISTANCE:
				self.camera_distance = config.MINIMUM_CAMERA_DISTANCE
		if button == 5:
			self.camera_distance *= config.CAMERA_DISTANCE_SCROLL
			if self.camera_distance > config.MAXIMUM_CAMERA_DISTANCE:
				self.camera_distance = config.MAXIMUM_CAMERA_DISTANCE
	
	def handle_key_down(self, key):
		# On ESC,  just return to the main menu
		if key == pygame.K_ESCAPE:
			self.return_to_menu = True
			self.pause_level()
			return True
		if key == pygame.K_F1:
			self.show_help_screen()
		elif self.ended and key == pygame.K_SPACE:
			self.end_level()	
		return super(SceneLevel, self).handle_key_down(key)
