"""
Module: person
Purpose: Person and derived classes.
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import random
import math

from OpenGL import GL,GLUT
import pygame

import config
import gametime
import model
import citygen
from movingobject import MovingObject

def _distance_legal(person,pos):
	if not isinstance(person,Orazi):
		return True
	pp = config.current_level.player.get_position()
	distance = abs(pp[0]-pos[0]) + abs(pp[1]-pos[1])
	return distance >= config.MINIMUM_ORAZI_START_DISTANCE

### Class Person ###
class Person(MovingObject):
	def __init__(self,position):
		if position == None:
			tile_to_try = (-1,-1)
			while not (_distance_legal(self,tile_to_try) and config.current_level.is_road_at(*tile_to_try)):
				map_size = config.current_level.map_size
				tile_to_try = ( int(map_size[0]*random.random()), int(map_size[1]*random.random()))
			position = (tile_to_try[0]+0.5, tile_to_try[1]+0.5)
		MovingObject.__init__(self,position)
		if not hasattr(self, 'direction_vector'):
			self.direction_vector = None #initialized at find_next_tile()
		self.find_next_tile()
		self.last_frame_moved = None
		self.last_frame_drawn = None
		self.speed = config.get_person_speed()
		self.auto_move = True
		self.direction = 0 #angle in degrees
		self.color = config.random_color()
	
	def move(self,frameDelta):
		#verify this object isn't moved again this frame
		if self.last_frame_moved == gametime.getFrameCount():
			return False
		self.last_frame_moved = gametime.getFrameCount()
		
		if not self.auto_move:
			return
		
		(x,y) = self.get_position()
		yet_to_go_x = self.direction_vector[0]*(self.next_tile_position[0]-x)
		yet_to_go_y = self.direction_vector[1]*(self.next_tile_position[1]-y)
		if max(yet_to_go_x,yet_to_go_y) <= 0:
			self.find_next_tile()
			return
		multiplier = self.speed * frameDelta
		x += self.direction_vector[0] * multiplier
		y += self.direction_vector[1] * multiplier
		#if x < 0:
		#	x = 0
		#if y < 0:
		#	y = 0
		#if x >= config.current_level.map_size[0]:
		#	x = config.current_level.map_size[0]-0.001
		#if y >= config.current_level.map_size[1]:
		#	y = config.current_level.map_size[1]-0.001
		self.set_position((x,y))
	
	def _check_path(self):
		(x,y) = map(int,self.get_position())
		if x == self.target_tile[0] and y == self.target_tile[1]:
			return None
		dx = self.target_tile[0] - x
		dy = self.target_tile[1] - y
		directions_to_try = []
		if dx > 0: directions_to_try.append((1,0))
		if dx < 0: directions_to_try.append((-1,0))
		if dy > 0: directions_to_try.append((0,1))
		if dy < 0: directions_to_try.append((0,-1))
		if dy > dx:
			directions_to_try.reverse()
		for tile_direction in directions_to_try:
			tile_to_try = (tile_direction[0]+x,tile_direction[1]+y)
			if config.current_level.is_road_at(*tile_to_try):
				return (tile_to_try,tile_direction)
		return None
		
	def find_next_tile(self):
		next_tile_and_dir = None
		while next_tile_and_dir == None:
			self.generate_target_tile()
			next_tile_and_dir = self._check_path()
		self.next_tile_position = (next_tile_and_dir[0][0]+0.5, next_tile_and_dir[0][1]+0.5)
		self.direction_vector = next_tile_and_dir[1]
		self.direction = math.atan2(self.direction_vector[0],self.direction_vector[1]) * 180/math.pi + 90
	
	def generate_target_tile(self):
		map_size = config.current_level.map_size
		self.target_tile = ( int(map_size[0]*random.random()), int(map_size[1]*random.random()) )
	
	def draw(self):
		#verify this object isn't drawn again this frame
		if self.last_frame_drawn == gametime.getFrameCount():
			return False
		self.last_frame_drawn = gametime.getFrameCount()
		
		(x,y) = self.get_position()
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glEnable(GL.GL_LIGHTING)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,self.color)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,self.color)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_SPECULAR,(0.1,0.1,0.1))
		GL.glPushMatrix()
		GL.glTranslatef(x,y,0)
		GL.glRotatef(self.direction,0,0,1)
		GLUT.glutSolidCone(0.06,0.2,12,1)
		GL.glTranslatef(0,0,0.2)
		GLUT.glutSolidSphere(0.04,12,12)
		GL.glPopMatrix()

### Class Orazi ###
class Orazi(Person):
	def __init__(self,position):
		Person.__init__(self,position)
	
	def find_next_tile(self):
		(x,y) = self.get_position()
		x = int(x)
		y = int(y)
		if config.current_level.signs.has_key((x,y)):
			#set end of path as target_tile
			sign_direction = config.current_level.signs[(x,y)]
			sign_direction = config.sign_directions[sign_direction]
			self.target_tile = tuple(map(int.__add__,(x,y),sign_direction))
			while config.current_level.is_road_at( *tuple(map(int.__add__,self.target_tile,sign_direction)) ):
				self.target_tile = tuple(map(int.__add__,self.target_tile,sign_direction))
		
		Person.find_next_tile(self)
	
	def draw(self):
		#verify this object isn't drawn again this frame
		if self.last_frame_drawn == gametime.getFrameCount():
			return False
		self.last_frame_drawn = gametime.getFrameCount()
		
		(x,y) = self.get_position()
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glEnable(GL.GL_LIGHTING)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_SPECULAR,(0.2,0.2,0.2))
		GL.glPushMatrix()
		GL.glTranslatef(x,y,0)
		GL.glRotatef(self.direction,0,0,1)
		
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,(0.7,0.7,0.7))
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.7,0.7,0.7))
		GLUT.glutSolidCone(0.06,0.2,12,1)
		
		GL.glTranslatef(0,0,0.2)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,(0.5,0.34,0.34))
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.5,0.34,0.34))
		GLUT.glutSolidSphere(0.04,12,12)
		
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,(0.0,0.25,0.0))
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.0,0.0,0.0))
		GL.glPushMatrix()
		GL.glTranslatef(0,0.02,0.04)
		GL.glRotatef(-60,1,0,0)
		GL.glScalef(0.04,0.02,0.01)
		GLUT.glutSolidCube(1)
		GL.glPopMatrix()
		
		GL.glTranslatef(0,-0.01,0.04)
		GL.glRotatef(30,1,0,0)
		GL.glScalef(0.04,0.065,0.01)
		GLUT.glutSolidCube(1)
		GL.glPopMatrix()
		
### Class WelcomeOrazi ###
class WelcomeOrazi(Orazi):
	def __init__(self, position):
		# Set direction according to position
		x, y = position
		if (y < 6) and (config.current_level.is_road_at(x+1, y)):
			self.direction_vector = (1, 0) # We go right
		elif (y >= 6) and (config.current_level.is_road_at(x-1, y)):
			self.direction_vector = (-1, 0) # We go left
		elif (y < 6) and (config.current_level.is_road_at(x, y-1)):
			self.direction_vector = (0, 1) # We go down
		else:
			self.direction_vector = (0, -1) # We go up
		super(WelcomeOrazi, self).__init__(position)

	def find_next_tile(self):
		(x,y) = map(int,self.get_position())

		dx, dy = self.direction_vector
		tile_to_try = (x + dx, y + dy)
		if not config.current_level.is_road_at(*tile_to_try):
			# Reverse direction, clockwise
			dx, dy = -dy, dx
			self.direction_vector = (dx, dy)
			#return self.find_next_tile()
		
		# We got a road here, update next tile
		self.next_tile_position = (tile_to_try[0]+0.5, tile_to_try[1]+0.5)
		self.direction = math.atan2(self.direction_vector[0],self.direction_vector[1]) * 180/math.pi + 90

### Class PlayerPerson ###
class PlayerPerson(Person):
	def __init__(self,position):
		Person.__init__(self,position)
		self.auto_move = False
		self.speed = config.player_speed

	def move(self,frameDelta):
		if None != Person.move(self,frameDelta):
			return #returns false if the person was moved already
		
		keys = pygame.key.get_pressed()
		forward = 0
		if keys[pygame.K_UP]:
			forward += 1.0
		if keys[pygame.K_DOWN]:
			forward -= 0.5
		if forward != 0:
			(x,y) = self.get_position()
			multiplier = forward * self.speed * frameDelta
			rad_direction = self.direction*math.pi/180
			x_add = math.cos(rad_direction) * multiplier
			y_add = math.sin(rad_direction) * multiplier
			if config.current_level.is_road_at(x+x_add,y+y_add):
				x += x_add
				y += y_add
				self.set_position((x,y))
			elif config.current_level.is_road_at(x+x_add,y):
				x += x_add
				y_add = 0
				self.set_position((x,y))
			elif config.current_level.is_road_at(x,y+y_add):
				x_add = 0
				y += y_add
				self.set_position((x,y))
			else:
				pass
		if keys[pygame.K_RIGHT]:
			self.direction -= config.rotation_speed * frameDelta
		if keys[pygame.K_LEFT]:
			self.direction += config.rotation_speed * frameDelta

### Class Frienda ###
class Frienda(PlayerPerson):
	def draw(self):
		#verify this object isn't drawn again this frame
		if self.last_frame_drawn == gametime.getFrameCount():
			return False
		self.last_frame_drawn = gametime.getFrameCount()
		
		(x,y) = self.get_position()
		GL.glDisable(GL.GL_TEXTURE_2D)
		GL.glEnable(GL.GL_LIGHTING)
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_SPECULAR,(0.2,0.2,0.2))
		GL.glPushMatrix()
		GL.glTranslatef(x,y,0)
		GL.glRotatef(self.direction,0,0,1)
		
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,(0.4,0.3,0.3))
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.4,0.3,0.3))
		GLUT.glutSolidCone(0.06,0.2,12,1)
		
		GL.glTranslatef(0,0,0.2)
		GLUT.glutSolidSphere(0.04,12,12)
		
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_AMBIENT,(0.4,0.3,0.3))
		GL.glMaterialfv(GL.GL_FRONT_AND_BACK,GL.GL_DIFFUSE,(0.0,0.0,0.0))
		
		GL.glPushMatrix()
		GL.glTranslatef(0,0.04,0)
		GL.glRotatef(-76,1,0,0)
		GL.glScalef(0.02,0.04,0.01)
		GLUT.glutSolidCube(1)
		GL.glPopMatrix()
		
		GL.glPushMatrix()
		GL.glTranslatef(0,-0.04,0)
		GL.glRotatef(76,1,0,0)
		GL.glScalef(0.02,0.04,0.01)
		GLUT.glutSolidCube(1)
		GL.glPopMatrix()
		
		GL.glPopMatrix()

### Class InvisiblePlayerPerson ###
class InvisiblePlayerPerson(Person):
	def draw(self):
		pass

### Global Functions ###
def create_random_person(person_type = Person, position = None):
	return person_type(position)
