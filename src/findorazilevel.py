"""
Module: FindOraziLevel
Purpose: Defines the game level of 'Find Orazi'
Author: Nir Hershko
Revision: $Id$
"""

### Imports ###
import pygame
from pgu import gui, html

import gametime
from scenelevel import SceneLevel
import person
import config
import levelintro
import sound

### Class FindOraziLevel ###
class FindOraziLevel(SceneLevel):
	def __init__(self):
		super(FindOraziLevel, self).__init__()
		self.intro_screen = levelintro.LevelIntro('findorazi', self)
		
		self.level_number = 1
		self.time_limit = config.FIND_ORAZI_TIME_LIMIT
		self.signs_in_inventory = 10
		self.map_size = (50,50) #run over scene level config
		
		# Create the GUI overlay
		self.createGui()
		self.resetGuiSurface()
		self.update_gui_content()
		
		self.lazyGui = True

	def createGui(self):
		self.gui = gui.App(theme=self.guiTheme)
		
		main = gui.Container(width=800, height=600)
		
		overlay = gui.Image("../media/LevelOverlay.png")
		
		overlayX, overlayY = 0, main.rect[3] - 40 #XXX
		main.add(overlay, overlayX, overlayY)
		
		self.signsLeftWidget = gui.Label('010')
		main.add(self.signsLeftWidget, overlayX + 45, overlayY + 12)
		self.timeLeftWidget = gui.Label('00:00')
		main.add(self.timeLeftWidget, overlayX + 100, overlayY + 12)
		
		self.gui.init(main)
	
	def updateOverlay(self, secondsLeft, signsLeft):
		self.signsLeftWidget.value = '%02d' % signsLeft
		self.timeLeftWidget.value = '%02d:%02d' % (secondsLeft / 60, secondsLeft % 60)
		self.redrawGui()
	
	def start(self):
		self.bgMusic = sound.SoundEffect.getSoundEffect(r'..\media\sound\march.ogg')
		SceneLevel.start(self)
		
		self.player = person.create_random_person(person.Frienda)
		
		for x in range(1):
			self.orazim.append(person.create_random_person(person.Orazi))
		
		#config.test_object = person.create_random_person()#Test
		self.bgMusic.playInfinite()
	
	def draw(self, frameDelta):
		self.look_at = self.player.get_position()
		
		SceneLevel.draw(self, frameDelta)
		
		if self.ended:
			return
		
		objects = self.tom.get_objects(*map(int,self.look_at))
		for o in objects:
			if isinstance(o,person.Orazi):
				self.win()
		
		if gametime.getLevelRunningTime() > self.time_limit:
			self.lose()

	def handle_key_down(self, key):
		SceneLevel.handle_key_down(self, key)
		
		if key == pygame.K_s:
			self._place_sign()
	
	def _place_sign(self):
		pos = self.player.get_position()
		pos = tuple(map(int,pos))
		if pos in self.signs:
			self.signs[pos] += 1
		else:
			if self.signs_in_inventory > 0:
				self.signs_in_inventory -= 1
				self.update_gui_content()
				self.signs[pos] = 0
			else:
				return
		
		if self.signs[pos] != 4:
			sd = config.sign_directions[self.signs[pos]]
		while self.signs[pos] != 4 and not self.is_road_at(pos[0]+sd[0],pos[1]+sd[1]):
			self.signs[pos] += 1
			if self.signs[pos] != 4:
				sd = config.sign_directions[self.signs[pos]]
		
		if self.signs[pos] == 4:
			del self.signs[pos]
			self.signs_in_inventory += 1
			self.update_gui_content()
	
	def generate_harder_level(self):
		new_level = self.__class__()
		new_level.map_size = (self.map_size[0]+20,self.map_size[1]+20)
		new_level.time_limit = self.time_limit * 0.9
		new_level.level_number = self.level_number + 1
		new_level.intro_screen = levelintro.LevelIntro('findorazi2', new_level)
		return new_level
	
	def update_gui_content(self):
		if self.ended:
			return
		timeleft = self.time_limit - gametime.getLevelRunningTime()
		self.updateOverlay(timeleft, self.signs_in_inventory)
